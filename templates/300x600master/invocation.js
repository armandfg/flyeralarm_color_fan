
//
// ────────────────────────────────────────────────────── I ──────────
//   :::::: D Y N A M I C : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────
// Dynamic invocation code.
// Use "Dynamic.get().key" to access the value.
// ie: var foo = Dynamic.get().ID;

var Dynamic = ( function () {

	function init () {

		Enabler.setProfileId(10610695);
		var devDynamicContent = {};
		
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed = [{}];
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0]._id = 0;
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].ID = 1;
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Reporting_Label = "Content_UnitedKingdom_300x250_vermeer_uk_1";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].creative_variation = "vermeer";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Country = "United Kingdom";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Language = "en-gb";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Headline1_Copy = "Our experience";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Headline2_Copy = "Your<br>Ambition";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Headline3_Copy = "";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].HeadlineEnd_CTA = "Let\'s advance<br>together";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Headline1_Setting = "{fontSize : 24px, lineHeight : auto, fontColor : #ffffff, yPos : 137px, xPos: 20px, oddEven: odd, indentValue : 10px}";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Headline2_Setting = "{fontSize : 24px, lineHeight : auto, fontColor : #ffffff, yPos : 137px, xPos: 20px, oddEven: odd, indentValue : 20px}";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Headline3_Setting = "{fontSize : 25px, lineHeight : auto, fontColor : #ffffff, yPos : 137px, xPos: 20px, oddEven: even, indentValue : 20px}";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].HeadlineEnd_CTA_Setting = "{fontSize : 21px, lineHeight : auto, fontColor : #ffffff, yPos : 146px, xPos: 20px, oddEven: even, indentValue : 17px}";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Subheadline_Copy = "Capital at risk";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Subheadline_Setting = "{fontSize : 17px, lineHeight : auto, fontColor : #ffffff, yPos : 220px, xPos: 190px}";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Frame3_Disclaimer = "View important information";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Frame_3_Legal = "Capital at risk. For professional investors only. Issued by Invesco Investment Management Limited, Central Quay, Riverside IV, Sir John Rogerson\u2019s Quay, Dublin 2, Ireland.\nEMEA6897\/2020";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Frame3_disclaimer_setting = "{fontSize : 12px, fontColor : #000000, yPos : 0px, xPos: 0px}";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Frame3_Legal_Setting = "{fontSize : 12px, lineHeight : auto, fontColor : #ffffff, bgColor: #0114d2, yPos : 0px, xPos: 0px}";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].BG_image = {};
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].BG_image.Type = "file";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].BG_image.Url = "images/vermeer_bg_image_300x250.jpg";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].BG_image_Anim_Setting = "{borderColor : #ffffff, borderSize : 5px,height: 250px, width: 300px, scaleFrom: 1.5,scaleTo: 1, initPosX: 40px, initPosY: 30px, finalPosX: 0px, finalPosY: 0px }";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Invesco_logo = {};
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Invesco_logo.Type = "file";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Invesco_logo.Url = "images/invesco_logo.png";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Invesco_logo_settings = "{height: 27px,width: 148px, xPos: 19px, yPos: 94px}";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].animated_strip_settings = "{backgroundColor: #0114d2, height: 75px, width: 300px, scaleUpValue: 5, bottom: 44px, left: 0px}";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Exit_URL = {};
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Exit_URL.Url = "https://www.invesco.com";
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Active = true;
		devDynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0].Default = false;
		Enabler.setDevDynamicContent(devDynamicContent);
	}

	function get () {
        
		return dynamicContent.CH_EN_Invesco_Brand_Reboot_Dynamic_2021_Feed[0];
	}

	return {
		init : init,
		get : get
	};
}() );