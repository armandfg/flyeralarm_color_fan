//
// ────────────────────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: D O U B L E C L I C K   B O I L E R P L A T E : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────────────
//

document.addEventListener( 'DOMContentLoaded', function () {

	if ( Enabler.isInitialized() ) {
		enablerInitHandler();
	} else {
		Enabler.addEventListener( studio.events.StudioEvent.INIT, enablerInitHandler );
	}

	function enablerInitHandler () {

		if ( Enabler.isPageLoaded() ) {
			pageLoadedHandler();
		} else {
			Enabler.addEventListener( studio.events.StudioEvent.PAGE_LOADED, pageLoadedHandler );
		}
	}

	function pageLoadedHandler () {

		if ( Enabler.isVisible() ) {
			adVisibilityHandler();
		} else {
			Enabler.addEventListener( studio.events.StudioEvent.VISIBLE, adVisibilityHandler );
		}
	}

	function adVisibilityHandler () {

		// Dynamic.init();
		Creative.init();
		
	}


	


} );








/*

{borderColor : #ffffff, borderSize : 5px, scaleFrom: 5, transformOrigin: top right, backgroundPosition: top right}

*/

//
// ──────────────────────────────────────────────────────────────── I ──────────
//   :::::: C R E A T I V E   D O M : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────
// Setup your creative's DOM access here.
// Use "DOM.get().selectorName" to access the value.
// ie: DOM.get().wrapper;



var DOM = ( function () {

	var el = {};

	function init () {

		el.wrapper = Utilities.selector( '.main-wrapper' );
		el.bg = Utilities.selector( '.bg' );
		

	}

	function get () {

		init();

		return el;
	}

	return {
		get : get
	};

}() );

//
// ──────────────────────────────────────────────────────── I ──────────
//   :::::: C R E A T I V E : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────
// This is the main entry point of your creative.
// Creative logic, animation, events, and everything to make your creative work.
var tl;

var Creative = ( function () {

	function init () {

		setup();
		
		addListeners();
	}
	
	//
	var tl;

	// 
	//
	// ───────────────────────────────────────────────────── SETUP DYNAMIC VALUES ─────
	// This is where you assign the dynamic values to your DOM elements.
    
	function addListeners() {

		// DOM.get().background_exit.addEventListener("click", function() {
		// 	Enabler.exitOverride("background_exit", Dynamic.get().Exit_URL.Url);

		// })

		// DOM.get().logo.addEventListener("click", function() {
		// 	Enabler.exitOverride("logo_exit", Dynamic.get().Exit_URL.Url)
			
		// })

		// DOM.get().cta_exit.addEventListener("click", function() {
		// 	Enabler.exitOverride("cta_exit", Dynamic.get().Exit_URL.Url);

			
		// })




		
	}

	function setup () {
        //INITIAL SETTINGS FOUND HERE

		//SET INITIAL STATES
		// TweenMax.set("#fan_1", {rotation: 73});

		TweenMax.set(".main-wrapper ", {opacity: 1});
		TweenMax.set("#end_frame_copy_1", {opacity: 0});
		TweenMax.set("#end_frame_copy_2", {opacity: 0});
		TweenMax.set("#end_frame_copy_3", {opacity: 0});

		TweenMax.set("#fan_parent", {opacity: 0});
		TweenMax.set("#top_left_shape", {x: -80, y: -80, opacity: 0});
		TweenMax.set("#bottom_right_shape", {x: 80, y: -80, opacity: 0});
		// SET TEXT POSITIONS
		setTimeout(function(){ 
			animate();
		}, 500);
		
	}

	//
	// ──────────────────────────────────────────────────────────────── ANIMATION ─────
	// This is where you'll place all animation related code.

	function animate () {
		TweenMax.set("#fan_parent", {opacity: 1});
		tl = new TimelineMax({});
		tl.staggerFrom(".fan_square", .7, {ease: "back.out(1.1)", transform:"rotate(0deg)"}, .2);
		
		
		tlEnd = new TimelineMax({delay: 3.5, paused: false});
		
		tlEnd.staggerTo(".fan_square", 1, { rotation: 300}, "f1 0.2");
		tlEnd.to("#fan_rotate", 1, { rotation: 160}, "f1");
		tlEnd.to("#fan_parent", 1, { autoAlpha: 0, y: 100}, "f1 -=1.5" );

		tlEnd.to("#top_left_shape",.5, {ease: "power2.inOut", x: 0, y: 0, opacity: 1}, "f2 -=1.5");
		tlEnd.to("#bottom_right_shape",.5, {ease: "power2.inOut", x: 0, y: 0, opacity:1}, "f2 -=1.5");
		tlEnd.to(".logo",.5, {ease: "power2.inOut", x: "-117px", y: "143px", opacity:1}, "f2 -=1.5");
		tlEnd.to("#end_frame_copy_1",1, {ease: "power2.inOut", opacity:1}, "-=1");
		tlEnd.to("#end_frame_copy_1",.5, {ease: "power2.inOut", opacity:0}, "txt2 +=1");
		tlEnd.to("#end_frame_copy_2",1, {ease: "power2.inOut", opacity:1}, "txt2 +=1");
		tlEnd.to("#end_frame_copy_2",.5, {ease: "power2.inOut", opacity:0}, "txt3 +=1");
		tlEnd.to("#end_frame_copy_3",1, {ease: "power2.inOut", opacity:1}, "txt3 +=1");
		// tlEnd.to("#end_frame_copy_3",.5, {ease: "power2.inOut", opacity:1}, "+=1.5");

	}   
	/* - -- - - - - -- -- - - - - -- -- - - - - -- -- - - - - -- -- - - - - -- -- - - - - -- -- - - - - -- -- - - - - -*/

	return {
		init : init
	};
}() );

//
// ────────────────────────────────────────────────────────── I ──────────
//   :::::: U T I L I T I E S : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────
//

var Utilities = ( function () {

	//
	// ─── QUERY SELECTOR ─────────────────────────────────────────────────────────────
	//
 
	function selector ( query ) {
 
		var t = document.querySelectorAll( query );
		return ( t.length === 0 ) ? false : ( t.length === 1 ) ? t[0] : t;
 
	}
    
	//
	// ─── TRIGGER CUSTOM EVENT ───────────────────────────────────────────────────────
	//

	function triggerEvent ( element, eventName, data ) {
		if ( document.createEvent ) {
			var event = document.createEvent( 'CustomEvent' );
			event.initCustomEvent( eventName, true, true, data );
		} else {
			// eslint-disable-next-line no-redeclare
			var event = new CustomEvent( eventName, { detail : data } );
		}
    
		element.dispatchEvent( event );
	}
	
	function normalizeJson ( str ) {
        return str.replace( /"?([\w_\- ]+)"?\s*?:\s*?"?(.*?)"?\s*?([,}\]])/gsi, ( str, index, item, end ) => '"' + index.replace( /"/gsi, '' ).trim() + '":"' + item.replace( /"/gsi, '' ).trim() + '"' + end ).replace( /,\s*?([}\]])/gsi, '$1' );
    }


	return {
 
		selector : selector,
		triggerEvent : triggerEvent,
		normalizeJson : normalizeJson
	};
 
} )();